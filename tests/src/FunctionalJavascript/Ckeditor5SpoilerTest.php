<?php

namespace Drupal\Tests\ckeditor5_spoiler\FunctionalJavascript;

use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\ckeditor5\Traits\CKEditor5TestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\user\RoleInterface;

/**
 * Defines tests for the ckeditor5 button and javascript functionality.
 *
 * @group ckeditor5_spoiler
 */
class Ckeditor5SpoilerTest extends WebDriverTestBase {

  use CKEditor5TestTrait;

  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'ckeditor5',
    'ckeditor5_spoiler',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalCreateContentType(['type' => 'page']);
    FilterFormat::create(
      [
        'format' => 'test',
        'name' => 'Ckeditor 5 with spoiler',
        'roles' => [RoleInterface::AUTHENTICATED_ID],
        'filters' => [
          'filter_html' => [
            'id' => 'filter_html',
            'status' => TRUE,
            'weight' => 2,
            'settings' => [
              'allowed_html' => '<br> <p> <div>',
              'filter_html_help' => TRUE,
              'filter_html_nofollow' => FALSE,
            ],
          ],
        ],
      ]
    )->save();
    Editor::create(
      [
        'format' => 'test',
        'editor' => 'ckeditor5',
        'settings' => [
          'toolbar' => [
            'items' => ['Spoiler', 'sourceEditing'],
          ],
        ],
      ]
    )->save();

    $this->drupalLogin(
      $this->drupalCreateUser(
        [
          'create page content',
          'edit own page content',
          'access content',
          'use text format test',
        ]
      )
    );

  }

  /**
   * Tests if CKEditor 5 spoilers can be interacted with in dialogs.
   */
  public function testCkeditor5Spoiler() {
    $assert_session = $this->assertSession();

    // Add a node with text rendered via the Plain Text format.
    $this->drupalGet('node/add');

    $this->waitForEditor();
    // Ensure the editor is loaded.
    $this->click('.ck-content');

    $this->assertEditorButtonEnabled('Spoiler');
    $this->click('.ck-button');

    $assert_session->waitForElement('css', '.spoiler');

    $this->click('.ck-source-editing-button');
    $source_text_area = $assert_session->waitForElement('css', '.ck-source-editing-area textarea');
    $this->assertNotEmpty($source_text_area);

    $raw_value = $source_text_area->getValue();
    $raw_value = str_replace(["\n", "  "], '', $raw_value);

    $this->assertEquals('<div class="spoiler"><div class="spoiler-title">Spoiler title<div class="spoiler-toggle hide-icon">&nbsp;</div></div><div class="spoiler-content"><p>&nbsp;</p></div></div>', $raw_value);
  }

}
