# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

This module enables the Spoiler plugin from CKEditor.com in your WYSIWYG.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/ckeditor5_spoiler>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/ckeditor5_spoiler>


## REQUIREMENTS

CKEditor 5 core


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configure > Content > Formats.
    3. Edit your text format and add Spoiler button to CKEditor toolbar.
    4. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
