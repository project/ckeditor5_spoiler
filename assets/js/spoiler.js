(($, Drupal, once) => {
  Drupal.behaviors.ckeditorSpoiler = {
    attach(context) {
      $(once('spoiler-title', 'div.spoiler-title', context))
        .not('.ck-editor__editable')
        // eslint-disable-next-line
        .click(function () {
          $(this).children().toggleClass('show-icon').toggleClass('hide-icon');
          $(this).parent().children().last().toggle();
        });
    },
  };
})(jQuery, Drupal, once);
